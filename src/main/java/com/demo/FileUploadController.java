package com.demo;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
@Controller
public class FileUploadController {

  @ResponseBody
  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public String upload(@RequestParam("file") MultipartFile multipartFile) {
    log.info("### uploaded : {}", multipartFile.getOriginalFilename());
    File targetFile = new File("/tmp/3scale-file-receiver-" + multipartFile.getOriginalFilename());
    try {
      InputStream fileStream = multipartFile.getInputStream();
      FileUtils.copyInputStreamToFile(fileStream, targetFile);
    } catch (IOException e) {
      FileUtils.deleteQuietly(targetFile);
      log.warn("Fail to save the uploaded file.", e);
    }
    return "OK";
  }
}